### ppt2pdf工具简介
* 这是一个批量PPT/PPS文件转PDF的工具。
* 仅适用于Windows平台,python 3.X环境。

### ppt2pdf.exe 使用方法
* 假设要转换的ppt文件在D:\file\ppt-dir目录下，将ppt2pdf.exe拷贝到D:\file\目录下.
* 通过命令行进入该文件夹,运行:

  cd D:

  cd file

  .\ppt2pdf.exe ppt-dir

### 运行条件
* Windows64位操作系统，需安装好PowerPoint，同时PowerPoint需安装另存为pdf的插件(如果没有请自行搜索“SaveAsPdfAndXps”)。

###  欢迎交流 
* cblock@126.com 
* 牛咕噜大人